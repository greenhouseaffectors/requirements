FROM ubuntu:latest
LABEL maintainer="michaellundquist7@gmail.com"

RUN apt-get -yq install apt-transport-https
RUN apt-get -yq update && apt-get -yq upgrade

#timezone stuff
RUN apt-get -y install tzdata
ENV TZ=America/New_York
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
#RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

#ubuntu dependencies
RUN apt-get -yq install apt-transport-https git gnuplot graphviz python3-distutils python3-pip
RUN apt-get -yq install texlive-font-utils texlive-latex-extra texlive-latex-recommended

#downloading and installing rmtoo
RUN git clone --single-branch --branch gitlabCI https://github.com/user-name-is-taken/rmtoo.git /usr/share/doc/rmtoo

RUN python3 -m pip install -r /usr/share/doc/rmtoo/requirements.txt
RUN python3 -m pip install /usr/share/doc/rmtoo 