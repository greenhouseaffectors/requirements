# Understanding the Requirements Repository

## Programs to Download

[**Git**](https://git-scm.com/downloads)
[**Visual Studio Code**](https://code.visualstudio.com/)

## RMToo weird stuff

- Effort estimation must be one of the following: `[0, 1, 2, 3, 5, 8, 13, 21, 34]`
- Every requirement must be either a `master` requirement (`topic_root_node` in `Config.json`) or a member of a `Solved by` relationship
